# sample-lambda-api

- hello_world - Code for the application's Lambda function.
- events - Invocation events that you can use to invoke the function.
- tests - Unit tests for the application code. 
- template.yaml - A template that defines the application's AWS resources.

##  Install and configure the SAM CLI.
*   refer [here](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)

##  Create an AWS serverless application using SAM CLI.
*   Let's create a hello world python app named `sample-lambda-api` by executing below command
    -   `sam init -r python3.8 -n sample-lambda-api --app-template "hello-world"`
    - or interactively `sam init`


##  Create the .gitlab-ci.yml file.
```yaml
image: python:3.8

variables:
  AWS_S3_BUCKET: ${AWS_S3_BUCKET}

stages:
  - deploy

production:
  stage: deploy
  before_script:
    - pip3 install awscli --upgrade
    - pip3 install aws-sam-cli --upgrade
  script:
    - sam build
    - sam package --output-template-file packaged.yaml --s3-bucket ${AWS_S3_BUCKET} --region us-east-1
    - sam deploy --template-file packaged.yaml --stack-name gitlabpoc  --s3-bucket ${AWS_S3_BUCKET} --capabilities CAPABILITY_IAM --region us-east-1

```

You can find your API Gateway Endpoint URL in the output values displayed after deployment in jobs log.

##  Set up your AWS credentials in your GitLab account.
*   Create a user with below permissions
    ```
    AWSCloudFormationFullAccess
    IAMFullAccess
    AWSLambda_FullAccess
    AmazonAPIGatewayAdministrator
    AmazonS3FullAccess
    AmazonEC2ContainerRegistryFullAccess - optional
    ```
 * set the creds and bucket in gitlab settings for cicd as variables.  
     ```
    AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY
    AWS_S3_BUCKET
    ``` 
##  Test locally.
- `sam build`
- `sam local invoke HelloWorldFunction -e events/event.json`
##  Deploy your application.
Git push the changes to  GitLab repository, and the GitLab pipeline will automatically deploy application.
##  Test your deployment.


